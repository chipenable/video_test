package ru.chipenable.videotest.model.screen;

import android.support.v4.app.Fragment;

import ru.chipenable.videotest.ui.fragment.MainFragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class MainScreen extends SupportAppScreen {

    @Override
    public Fragment getFragment() {
        return new MainFragment();
    }
}
