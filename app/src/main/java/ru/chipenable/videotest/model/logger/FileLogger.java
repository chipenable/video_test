package ru.chipenable.videotest.model.logger;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Locale;


public class FileLogger implements Runnable {

    private final Thread thread;
    private final ILogListener listener;
    private final SimpleDateFormat dateFormatter;
    private final File logDir;
    private File currentLog;
    private static final String TAG = "FileLogger";

    public interface ILogListener {
        void show(String msg);
    }

    public static File getLogDir(){

        File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File logDir = new File(file, "video_log");
        if (!logDir.exists()){
            logDir.mkdir();
        }

        return logDir;
    }

    public static FileLogger createInstance(){
        File logDir = getLogDir();
        return new FileLogger(logDir, null);
    }

    public FileLogger(File logDir, ILogListener listener){
        this.listener = listener;
        this.thread = new Thread(this);
        this.dateFormatter = new SimpleDateFormat("ddMMyy_HH-mm-ss", Locale.getDefault());
        this.logDir = logDir;
        this.currentLog = createLogFile(logDir);
    }

    public void addHeader(String header){
        addHeaderToLog(currentLog, header);
    }

    public void start(){
        thread.start();
    }

    public void stop(){
        thread.interrupt();
    }


    @Override
    public void run() {
        BufferedReader bufferedReader = null;
        FileOutputStream outputStream = null;

        try {
            Process process = Runtime.getRuntime().exec("logcat");
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            outputStream = new FileOutputStream(currentLog, true);
            byte[] separator = System.getProperty("line.separator").getBytes();


            while(true){
                String line = bufferedReader.readLine();
                if (line != null){
                    //if (line.contains("Player") || line.contains("OpenGL")) {
                        byte[] buffer = line.getBytes();
                        outputStream.write(buffer, 0, buffer.length);
                        outputStream.write(separator);
                    //}
                }

                if (thread.isInterrupted()){
                    break;
                }
            }

            process = Runtime.getRuntime().exec("logcat -c");
        }
        catch (IOException e){
            Log.d(TAG, "error: " + e);
        }

        closeStream(bufferedReader);
        closeStream(outputStream);


    }

    private File createLogFile(File parentDir){
        long time = System.currentTimeMillis();

        String logName = "videoTest_" + dateFormatter.format(time) + ".log";
        File file = new File(parentDir, logName);

        return file;
    }

    private void addHeaderToLog(File logFile, String header){
        if (header == null || header.isEmpty()){
            return;
        }

        try(FileOutputStream outputStream = new FileOutputStream(logFile)){
            byte [] buffer = header.getBytes( );
            outputStream.write(buffer, 0, buffer.length);
        }
        catch(Exception e) {
        }
    }

    private void closeStream(BufferedReader reader){
        if (reader == null){
            return;
        }

        try{
            reader.close();
        }
        catch (IOException e){

        }
    }

    private void closeStream(FileOutputStream stream){
        if (stream == null){
            return;
        }

        try{
            stream.close();
        }
        catch (IOException e){

        }
    }

}
