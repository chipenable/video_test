package ru.chipenable.videotest.model.screen;

import android.support.v4.app.Fragment;

import ru.chipenable.videotest.ui.fragment.LogListFragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class LogListScreen extends SupportAppScreen {

    @Override
    public Fragment getFragment() {
        return new LogListFragment();
    }
}
