package ru.chipenable.videotest.model.screen;

import android.support.v4.app.Fragment;

import ru.chipenable.videotest.ui.fragment.VideoViewFragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class PlayerScreen extends SupportAppScreen {

    private final String link;

    public PlayerScreen(String link){
        this.link = link;
    }

    @Override
    public Fragment getFragment() {
        return VideoViewFragment.createInstance(link);
        //return SurfaceViewFragment.createInstance(link);
    }
}
