package ru.chipenable.videotest.model.screen;

import android.support.v4.app.Fragment;

import ru.chipenable.videotest.ui.fragment.SourceFragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class SourceScreen extends SupportAppScreen {

    @Override
    public Fragment getFragment() {
        return new SourceFragment();
    }
}
