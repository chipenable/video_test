package ru.chipenable.videotest.model.screen;

import android.support.v4.app.Fragment;

import ru.chipenable.videotest.ui.fragment.LogFragment;
import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class LogScreen extends SupportAppScreen {

    private final String logName;

    public LogScreen(String logName){
        this.logName = logName;
    }

    @Override
    public Fragment getFragment() {
        return LogFragment.createInstance(logName);
    }
}
