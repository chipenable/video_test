package ru.chipenable.videotest.model.movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MovieParser {

    public List<Movie> parse(String content){
        List<Movie> result = new ArrayList<>();

        try {
            JSONArray jsonArray = new JSONArray(content);
            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject obj = jsonArray.getJSONObject(i);
                String title = obj.getString("name");
                JSONArray samples = obj.getJSONArray("samples");
                List<Movie> movies = parseSamples(title, samples);
                result.addAll(movies);
            }

        } catch (JSONException e) {

        }

        return result;
    }

    private List<Movie> parseSamples(String title, JSONArray jsonArray){
        List<Movie> result = new ArrayList<>();
        if (jsonArray == null){
            return result;
        }

        for(int i = 0; i < jsonArray.length(); i++){
            try {
                JSONObject obj = jsonArray.getJSONObject(i);
                String name = obj.getString("name");
                String url = obj.getString("uri");
                Movie movie = new Movie(String.format("%s %s", title, name), url);
                result.add(movie);

            } catch (JSONException e) {

            }

        }


        return result;
    }



}
