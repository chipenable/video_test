package ru.chipenable.videotest.model.movie;

public class Movie {

    private final String name;
    private final String url;

    public Movie(String name, String url){
        this.name = name;
        this.url = url;
    }

    public String getName(){
        return name;
    }

    public String getUrl(){
        return url;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
