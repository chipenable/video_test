package ru.chipenable.videotest.presentation.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import ru.chipenable.videotest.model.movie.Movie;

@StateStrategyType(OneExecutionStateStrategy.class)
public interface IVideoListView extends MvpView {

    void showLinks(List<Movie> list);

    void showLoading();

    void hideLoading();

}
