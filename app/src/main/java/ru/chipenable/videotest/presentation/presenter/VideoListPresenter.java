package ru.chipenable.videotest.presentation.presenter;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.chipenable.videotest.data.IMovieRepo;
import ru.chipenable.videotest.data.Settings;
import ru.chipenable.videotest.model.VideoSource;
import ru.chipenable.videotest.model.movie.Movie;
import ru.chipenable.videotest.model.screen.PlayerScreen;
import ru.chipenable.videotest.presentation.view.IVideoListView;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class VideoListPresenter extends MvpPresenter<IVideoListView> {

    private static final String TAG = "VideoListPresenter";
    private final Router router;
    private final IMovieRepo repo;
    private final Settings settings;
    private List<Movie> movies;
    private Disposable disp;

    public VideoListPresenter(Router router, IMovieRepo repo, Settings settings){
        this.router = router;
        this.repo = repo;
        this.settings = settings;
    }

    @Override
    public void attachView(IVideoListView view) {
        super.attachView(view);
        getViewState().showLinks(movies);

        VideoSource source = settings.getVideoSource();
        if (source == VideoSource.LOCAL_FILE){
            movies = repo.getLocalMovies();
            getViewState().showLinks(movies);
        }
        else if (source == VideoSource.URL){
            Log.d(TAG, "try to download movies");
            disp = repo.getRemoteMovies()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(
                            result -> {
                                Log.d(TAG, "result: " + result.size()); 
                                movies = new ArrayList<>();
                                getViewState().showLinks(movies);
                            },
                            throwable -> Log.d(TAG, "can't load movies: " + throwable.getMessage()),
                            () -> {}
                    );
        }

    }

    @Override
    public void detachView(IVideoListView view) {
        super.detachView(view);
        if (disp != null && !disp.isDisposed()){
            disp.dispose();
        }
    }

    public void openLink(int position){
        if (position >= movies.size() || position < 0){
            return;
        }

        String url = movies.get(position).getUrl();
        router.navigateTo(new PlayerScreen(url));
    }

}
