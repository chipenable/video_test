package ru.chipenable.videotest.data;

import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AssetLoader {

    private final AssetManager assetManager;

    public AssetLoader(AssetManager assetManager){
        this.assetManager = assetManager;
    }

    public String readAssetFile(String path){
        String result = "";

        try(
                InputStream is = assetManager.open(path);
                BufferedReader br = new BufferedReader(new InputStreamReader(is))
        ){
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine()) != null){
                sb.append(line);
            }

            result = sb.toString();

        } catch (IOException e) {

        }

        return result;
    }


}
