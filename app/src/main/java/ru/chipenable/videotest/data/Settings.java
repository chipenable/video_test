package ru.chipenable.videotest.data;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.UserHandle;

import ru.chipenable.videotest.model.VideoSource;

public class Settings {

    private final SharedPreferences preferences;
    private final String VIDEO_SOURCE_KEY = "video_source";
    private final String URL_KEY = "url_key";

    public Settings(Context context){
        this.preferences = context.getSharedPreferences("settings", Activity.MODE_PRIVATE);
    }

    public String getUrl(){
        return preferences.getString(URL_KEY, "");
    }

    public void saveUrl(String url){
        if (url == null || url.isEmpty()){
            return;
        }

        preferences.edit().putString(URL_KEY, url).commit();
    }

    public VideoSource getVideoSource(){
        String videoSourceStr = preferences.getString(VIDEO_SOURCE_KEY, "");

        VideoSource result = VideoSource.LOCAL_FILE;
        try {
            result = VideoSource.valueOf(videoSourceStr);
        }
        catch(Exception e){

        }

        return result;
    }

    public void saveVideoSource(VideoSource value){
        if (value == null){
            return;
        }

        String videoSourceStr = value.toString();
        preferences.edit()
                .putString(VIDEO_SOURCE_KEY, videoSourceStr)
                .commit();
    }

}
