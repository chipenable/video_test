package ru.chipenable.videotest.data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class BaseLoader {

    private final OkHttpClient client;
    private LogCallback logger;

    public interface LogCallback {
        void onLog(String msg);
    }

    public BaseLoader(OkHttpClient client, LogCallback logger){
        this.client = client;
        this.logger = logger;
    }

    public String loadDataAsString(String url) throws IOException, IllegalArgumentException {

        Request request;
        try {
            request = new Request.Builder()
                    .url(url)
                    .build();
        }
        catch(IllegalArgumentException | NullPointerException e){
            logger.onLog("bad request: " + e);
            throw new IllegalArgumentException("unexpected url: " + e.toString());
        }

        Response response = client.newCall(request).execute();
        if(!response.isSuccessful()){
            response.close();
            throw new IOException("Unexpected result: " + response.toString());
        }

        ResponseBody responseBody = response.body();
        if (responseBody == null){
            throw new IOException("Unexpected result: response body is null");
        }

        String result = "";
        try (
                InputStream in = responseBody.byteStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in))
        ) {

            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            result = sb.toString();
        }
        catch(IOException e){
            throw new IOException(e);
        }

        return result;
    }


    public boolean loadData(String url, File file) {

        boolean result = false;
        Request request;

        try {
            request = new Request.Builder()
                    .url(url)
                    .build();
        }
        catch(IllegalArgumentException | NullPointerException e){
            logger.onLog("bad request: " + e);
            return result;
        }

        Response response = null;
        try {
            response = client.newCall(request).execute();
        }
        catch(IOException e){
            return result;
        }

        ResponseBody responseBody = response.body();
        if (!response.isSuccessful() || responseBody == null){
            close(response);
            return result;
        }

        try (
                OutputStream os = new FileOutputStream(file);
                InputStream is = responseBody.byteStream()
        ) {
            final int BUFFER_SIZE = 64 * 1024;
            byte[] bytes = new byte[BUFFER_SIZE];

            int downloadedBytes;
            while((downloadedBytes = is.read(bytes)) != -1) {
                os.write(bytes, 0, downloadedBytes);
            }

            result = true;

        }
        catch(NullPointerException | IOException e){
            return result;
        }


        return result;
    }

    private void close(Response response){
        if (response == null){
            return;
        }

        try{
            response.close();
        }
        catch (IllegalStateException e){

        }
    }

}
