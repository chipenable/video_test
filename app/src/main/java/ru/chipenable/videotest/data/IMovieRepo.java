package ru.chipenable.videotest.data;

import java.util.List;

import io.reactivex.Observable;
import ru.chipenable.videotest.model.movie.Movie;

public interface IMovieRepo {

    List<Movie> getLocalMovies();
    Observable<List<Movie>> getRemoteMovies();

}
