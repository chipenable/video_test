package ru.chipenable.videotest.data;

import java.util.List;
import java.util.Set;

import io.reactivex.Observable;
import ru.chipenable.videotest.model.movie.Movie;
import ru.chipenable.videotest.model.movie.MovieParser;

public class MovieRepo implements IMovieRepo {

    private final AssetLoader assetLoader;
    private final MovieParser movieParser;
    private final BaseLoader baseLoader;
    private final Settings settings;

    public MovieRepo(AssetLoader assetLoader, MovieParser movieParser, BaseLoader baseLoader, Settings settings){
        this.assetLoader = assetLoader;
        this.movieParser = movieParser;
        this.baseLoader = baseLoader;
        this.settings = settings;
    }

    public List<Movie> getLocalMovies(){
        String content = assetLoader.readAssetFile("links.json");
        return movieParser.parse(content);
    }

    public Observable<List<Movie>> getRemoteMovies(){
        String url = settings.getUrl();
        return Observable.fromCallable(() -> {
            String content = baseLoader.loadDataAsString(url);
            return movieParser.parse(content);
        });
    }

}
