package ru.chipenable.videotest.ui.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import ru.chipenable.videotest.App;
import ru.chipenable.videotest.BuildConfig;
import ru.chipenable.videotest.R;
import ru.chipenable.videotest.model.screen.MainScreen;
import ru.chipenable.videotest.model.screen.VideoListScreen;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;

public class MainActivity extends AppCompatActivity {


    private static final int PERMISSIONS_REQUEST_WRITE_STORAGE = 1;
    private Navigator navigator = new SupportAppNavigator(this, R.id.fragment_container){

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getApp().getRouter().newRootScreen(new MainScreen());
    }

    @Override
    protected void onResume() {
        super.onResume();
        getApp().getNavigatorHolder().setNavigator(navigator);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        PERMISSIONS_REQUEST_WRITE_STORAGE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        getApp().getNavigatorHolder().removeNavigator();
    }

    private App getApp(){
        return (App)getApplication();
    }

}
