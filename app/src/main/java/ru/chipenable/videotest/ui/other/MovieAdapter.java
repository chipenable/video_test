package ru.chipenable.videotest.ui.other;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.chipenable.videotest.R;
import ru.chipenable.videotest.model.movie.Movie;

public class MovieAdapter extends BaseAdapter {

    private List<Movie> list;

    public MovieAdapter(){
        list = new ArrayList<>();
    }

    public void setData(List<Movie> list){
        if (list == null){
            return;
        }

        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.movie_item, parent, false);
        }

        Movie movie = list.get(position);
        TextView nameView = convertView.findViewById(R.id.name_view);
        nameView.setText(movie.getName());

        return convertView;
    }
}
