package ru.chipenable.videotest.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RadioButton;

import java.util.ArrayList;
import java.util.List;

import ru.chipenable.videotest.App;
import ru.chipenable.videotest.R;
import ru.chipenable.videotest.data.Settings;
import ru.chipenable.videotest.model.VideoSource;

/**
 * A simple {@link Fragment} subclass.
 */
public class SourceFragment extends Fragment {

    private RadioButton internalSourceBut;
    private RadioButton externalSourceBut;
    private AutoCompleteTextView urlView;
    private Settings settings;
    private VideoSource source;

    public SourceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_source, container, false);

        Toolbar toolbar = view.findViewById(R.id.toolbar_view);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle("Источник");

        settings = ((App)(getActivity().getApplication())).getSettings();
        source = settings.getVideoSource();

        urlView = view.findViewById(R.id.video_url_view);
        List<String> urls = new ArrayList<>();
        urls.add(settings.getUrl());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_dropdown_item_1line, urls);
        urlView.setAdapter(adapter);
        urlView.setText(settings.getUrl());
        urlView.setOnClickListener(v -> {
            InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        });

        internalSourceBut = view.findViewById(R.id.internal_source_but);
        externalSourceBut = view.findViewById(R.id.external_source_but);
        if (source == VideoSource.LOCAL_FILE){
            internalSourceBut.setChecked(true);
            externalSourceBut.setChecked(false);
        }
        else if (source == VideoSource.URL){
            internalSourceBut.setChecked(false);
            externalSourceBut.setChecked(true);
        }

        internalSourceBut.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked){
                source = VideoSource.LOCAL_FILE;
                settings.saveVideoSource(source);
            }
        });

        externalSourceBut.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(isChecked){
                source = VideoSource.URL;
                settings.saveVideoSource(source);
            }
        });


        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        settings.saveUrl(urlView.getText().toString());
    }
}
