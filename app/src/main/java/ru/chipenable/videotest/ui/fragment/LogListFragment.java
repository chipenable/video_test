package ru.chipenable.videotest.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import ru.chipenable.videotest.App;
import ru.chipenable.videotest.R;
import ru.chipenable.videotest.model.logger.FileLogger;
import ru.chipenable.videotest.model.screen.LogScreen;
import ru.terrakok.cicerone.Router;

/**
 * A simple {@link Fragment} subclass.
 */
public class LogListFragment extends Fragment {

    private Router router;
    private List<String> logList;

    public LogListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_log_list, container, false);

        router = getApp().getRouter();

        Toolbar toolbar = view.findViewById(R.id.toolbar_view);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle("Лог");

        ListView listView = view.findViewById(R.id.log_list_view);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1);

        logList = getLogList();
        adapter.addAll(logList);
        adapter.notifyDataSetChanged();

        listView.setAdapter(adapter);

        listView.setOnItemClickListener((parent, view1, position, id) -> {
            router.navigateTo(new LogScreen(logList.get(position)));
        });

        return view;
    }


    private List<String> getLogList(){
        File logDir = FileLogger.getLogDir();
        File[] files = logDir.listFiles();
        if (files == null){
            files = new File[0];
        }

        Arrays.sort(files, (o1, o2) -> Long.compare(o2.lastModified(), o1.lastModified()));

        List<String> result = new ArrayList<>();
        for(File f: files){
            result.add(f.getName());
        }
        return result;
    }

    private App getApp(){
        return (App)(getActivity().getApplication());
    }

}
