package ru.chipenable.videotest.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ru.chipenable.videotest.App;
import ru.chipenable.videotest.R;
import ru.chipenable.videotest.model.screen.LogListScreen;
import ru.chipenable.videotest.model.screen.SourceScreen;
import ru.chipenable.videotest.model.screen.VideoListScreen;
import ru.terrakok.cicerone.Router;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {


    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        Toolbar toolbar = view.findViewById(R.id.toolbar_view);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle("Video test");

        Router router = getApp().getRouter();

        Button sourceView = view.findViewById(R.id.source_view);
        sourceView.setOnClickListener(v -> router.navigateTo(new SourceScreen()));
        Button playerView = view.findViewById(R.id.player_view);
        playerView.setOnClickListener(v -> router.navigateTo(new VideoListScreen()));
        Button logView = view.findViewById(R.id.log_view);
        logView.setOnClickListener(v -> router.navigateTo(new LogListScreen()));

        return view;
    }

    private App getApp(){
        return (App)(getActivity().getApplication());
    }

}
