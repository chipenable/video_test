package ru.chipenable.videotest.ui.fragment;


import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;

import java.io.IOException;

import ru.chipenable.videotest.R;
import ru.chipenable.videotest.model.logger.FileLogger;

/**
 * A simple {@link Fragment} subclass.
 */
public class SurfaceViewFragment extends Fragment implements SurfaceHolder.Callback,
        MediaPlayer.OnPreparedListener, MediaController.MediaPlayerControl {

    private FileLogger logger;
    private String url = "";
    private MediaPlayer mediaPlayer;
    private SurfaceView surfaceView;

    private static final String TAG = "VideoViewFragment";
    private static final String MOVIE_URL_KEY = "movie_url";

    public static SurfaceViewFragment createInstance(String url){
        Bundle args = new Bundle();
        args.putString(MOVIE_URL_KEY, url);
        SurfaceViewFragment f = new SurfaceViewFragment();
        f.setArguments(args);
        return f;
    }

    public SurfaceViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null){
            url = args.getString(MOVIE_URL_KEY, "");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_surface_view, container, false);

        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();
        } catch (IOException e) {

        }

        surfaceView = view.findViewById(R.id.surface_view);
        surfaceView.getHolder().addCallback(this);

        MediaController mediaController = new MediaController(getActivity());
        mediaController.setAnchorView(view);
        mediaController.setMediaPlayer(this);
        mediaController.show();

        logger = FileLogger.createInstance();
        logger.addHeader("VideoFile: " + url + "\n\n");
        logger.start();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        logger.stop();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (mediaPlayer != null) {
            mediaPlayer.setDisplay(holder);
            mediaPlayer.start();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {

    }

    @Override
    public void start() {
        mediaPlayer.start();
    }

    @Override
    public void pause() {
        mediaPlayer.pause();

    }

    @Override
    public int getDuration() {
        return mediaPlayer.getDuration();
    }

    @Override
    public int getCurrentPosition() {
        return mediaPlayer.getCurrentPosition();
    }

    @Override
    public void seekTo(int pos) {
        mediaPlayer.seekTo(pos);
    }

    @Override
    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return false;
    }

    @Override
    public boolean canSeekForward() {
        return false;
    }

    @Override
    public int getAudioSessionId() {
        return mediaPlayer.getAudioSessionId();
    }
}
