package ru.chipenable.videotest.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import ru.chipenable.videotest.R;
import ru.chipenable.videotest.model.logger.FileLogger;

/**
 * A simple {@link Fragment} subclass.
 */
public class LogFragment extends Fragment {

    private String logName;
    private static final String LOG_NAME_KEY = "log_name";

    public static LogFragment createInstance(String logName){
        Bundle args = new Bundle();
        args.putString(LOG_NAME_KEY, logName);
        LogFragment f = new LogFragment();
        f.setArguments(args);
        return f;
    }

    public LogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null){
            logName = args.getString(LOG_NAME_KEY, "");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_log, container, false);

        Toolbar toolbar = view.findViewById(R.id.toolbar_view);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle("Лог: " + logName);

        TextView logView = view.findViewById(R.id.log_view);
        showLog(logView);

        return view;
    }

    private void showLog(TextView view){
        File logDir = FileLogger.getLogDir();
        File log = new File(logDir, logName);
        try(
                FileInputStream fis = new FileInputStream(log);
                BufferedReader br = new BufferedReader(new InputStreamReader(fis));
        ){
            String line = "";
            StringBuilder sb = new StringBuilder();
            while((line = br.readLine()) != null){
                sb.append(line).append("\n");
            }
            view.setText(sb.toString());
        }
        catch (IOException e){
            view.setText("can't read log file: " + e.toString());
        }
    }

}
