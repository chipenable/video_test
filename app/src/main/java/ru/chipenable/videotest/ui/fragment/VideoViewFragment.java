package ru.chipenable.videotest.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.VideoView;


import ru.chipenable.videotest.R;
import ru.chipenable.videotest.model.logger.FileLogger;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoViewFragment extends Fragment {

    private String url;
    private VideoView videoView;
    private FileLogger logger;

    private static final String TAG = "VideoViewFragment";
    private static final String MOVIE_URL_KEY = "movie_url";

    public static VideoViewFragment createInstance(String url){
        Bundle args = new Bundle();
        args.putString(MOVIE_URL_KEY, url);
        VideoViewFragment f = new VideoViewFragment();
        f.setArguments(args);
        return f;
    }

    public VideoViewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null){
            url = args.getString(MOVIE_URL_KEY, "");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_player, container, false);
        videoView = view.findViewById(R.id.video_view);
        videoView.setVideoPath(url);

        MediaController mediaController = new MediaController(getActivity());
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.start();

        logger = FileLogger.createInstance();
        logger.addHeader("VideoFile: " + url + "\n\n");
        logger.start();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        logger.stop();
        if (videoView != null){
            videoView.stopPlayback();
        }
    }


}
