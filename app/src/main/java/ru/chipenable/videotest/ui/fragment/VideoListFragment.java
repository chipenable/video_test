package ru.chipenable.videotest.ui.fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.List;

import ru.chipenable.videotest.App;
import ru.chipenable.videotest.R;
import ru.chipenable.videotest.data.IMovieRepo;
import ru.chipenable.videotest.data.Settings;
import ru.chipenable.videotest.model.movie.Movie;
import ru.chipenable.videotest.presentation.presenter.VideoListPresenter;
import ru.chipenable.videotest.presentation.view.IVideoListView;
import ru.chipenable.videotest.ui.other.MovieAdapter;
import ru.terrakok.cicerone.Router;


public class VideoListFragment extends MvpAppCompatFragment implements IVideoListView {

    private ListView videoListView;
    private MovieAdapter adapter;

    @InjectPresenter
    public VideoListPresenter presenter;

    @ProvidePresenter
    public VideoListPresenter providePresenter(){
        Router router = getApp().getRouter();
        IMovieRepo repo = getApp().getMovieRepo();
        Settings settings = getApp().getSettings();
        return new VideoListPresenter(router, repo, settings);
    }

    public VideoListFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_video_list, container, false);

        Toolbar toolbar = view.findViewById(R.id.toolbar_view);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        videoListView = view.findViewById(R.id.video_list_view);
        adapter = new MovieAdapter();
        videoListView.setAdapter(adapter);
        videoListView.setOnItemClickListener((parent, view1, position, id) -> {
            presenter.openLink(position);
        });

        return view;
    }

    @Override
    public void showLinks(List<Movie> list) {
        adapter.setData(list);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    private App getApp(){
        return (App)(getActivity().getApplication());
    }

}
