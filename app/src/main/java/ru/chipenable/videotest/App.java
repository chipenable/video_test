package ru.chipenable.videotest;

import android.app.Application;
import android.util.Log;

import java.util.Set;

import okhttp3.OkHttpClient;
import okhttp3.internal.tls.OkHostnameVerifier;
import ru.chipenable.videotest.data.AssetLoader;
import ru.chipenable.videotest.data.BaseLoader;
import ru.chipenable.videotest.data.IMovieRepo;
import ru.chipenable.videotest.data.MovieRepo;
import ru.chipenable.videotest.data.Settings;
import ru.chipenable.videotest.model.movie.MovieParser;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

public class App extends Application {

    private Cicerone<Router> cicerone;
    private OkHttpClient httpClient;
    private IMovieRepo movieRepo;
    private Settings settings;


    @Override
    public void onCreate() {
        super.onCreate();

        settings = new Settings(this);
        httpClient = new OkHttpClient();
        cicerone = Cicerone.create();

        AssetLoader assetLoader = new AssetLoader(getAssets());
        BaseLoader baseLoader = new BaseLoader(httpClient, msg -> {
            Log.d("HttpClient", (msg == null? "null" : msg));
        });
        movieRepo = new MovieRepo(assetLoader, new MovieParser(), baseLoader, settings);

    }


    public Router getRouter(){
        return cicerone.getRouter();
    }

    public NavigatorHolder getNavigatorHolder(){
        return cicerone.getNavigatorHolder();
    }

    public IMovieRepo getMovieRepo() {
        return movieRepo;
    }

    public Settings getSettings(){
        return settings;
    }
}
